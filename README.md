# Soong\Transformer

[![Latest Version on Packagist][ico-version]][link-packagist]
[![Software License][ico-license]](LICENSE.md)
[![Build Status][ico-travis]][link-travis]
[![Coverage Status][ico-scrutinizer]][link-scrutinizer]
[![Quality Score][ico-code-quality]][link-code-quality]
[![Total Downloads][ico-downloads]][link-downloads]

Soong\Transformer provides basic transformers for the Soong ETL framework.

## Install

Via Composer

``` bash
$ composer require soong/transformer
```

## Usage


## Change log

Please see [CHANGELOG](CHANGELOG.md) for more information on what has changed recently.

## Testing

``` bash
$ composer test
```

## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) and [CODE_OF_CONDUCT](CODE_OF_CONDUCT.md) for details.

### Todo
* Complete test coverage

## Security

If you discover any security related issues, please email `soong@virtuoso-performance.com` instead of using the issue tracker.

## Credits

- [Mike Ryan][link-author]
- [All Contributors][link-contributors]

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.

[ico-version]: https://img.shields.io/packagist/v/soong/transformer.svg?style=flat-square
[ico-license]: https://img.shields.io/badge/license-MIT-brightgreen.svg?style=flat-square
[ico-travis]: https://img.shields.io/travis/soong/transformer/master.svg?style=flat-square
[ico-scrutinizer]: https://img.shields.io/scrutinizer/coverage/g/soong/transformer.svg?style=flat-square
[ico-code-quality]: https://img.shields.io/scrutinizer/g/soong/transformer.svg?style=flat-square
[ico-downloads]: https://img.shields.io/packagist/dt/soong/transformer.svg?style=flat-square

[link-packagist]: https://packagist.org/packages/soong/transformer
[link-travis]: https://travis-ci.org/soong/transformer
[link-scrutinizer]: https://scrutinizer-ci.com/g/soong/transformer/code-structure
[link-code-quality]: https://scrutinizer-ci.com/g/soong/transformer
[link-downloads]: https://packagist.org/packages/soong/transformer
[link-author]: https://gitlab.com/mikeryan776
[link-contributors]: ../../contributors
