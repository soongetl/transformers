<?php
declare(strict_types=1);

namespace Soong\Transformer;

use Soong\Data\DataPropertyInterface;

/**
 * Transformer to simply copy extracted data to the destination.
 *
 * @package Soong\Transformer
 */
class Copy implements TransformerInterface
{

    /**
     * {@inheritdoc}
     */
    public function transform(array $configuration, ?DataPropertyInterface $data) : ?DataPropertyInterface
    {
        return $data;
    }
}
