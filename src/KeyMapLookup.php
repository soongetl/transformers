<?php
declare(strict_types=1);

namespace Soong\Transformer;

use Soong\Data\Property;
use Soong\Data\DataPropertyInterface;
use Soong\Task\EtlTask;

/**
 * Transformer accepting a unique key from the extracted data and looking up the
 * key of any data loaded from it.
 *
 * Configuration:
 *   key_map:
 *     task_id: Unique identifier of the EtlTask which migrated the data.
 *
 * @package Soong\Transformer
 */
class KeyMapLookup implements TransformerInterface
{

    /**
     * {@inheritdoc}
     */
    public function transform(array $configuration, ?DataPropertyInterface $data) : ?DataPropertyInterface
    {
        if (!is_null($data)) {
            $keyMapConfig = $configuration['key_map'];
            /** @var \Soong\Task\EtlTaskInterface $task */
            $task = EtlTask::getTask($keyMapConfig['task_id']);
            // @todo: Allow multiple key maps.
            $keyMap = $task->getKeyMap();
            $loadedKey = $keyMap->lookupLoadedKey([$data->getValue()]);
            if (!empty($loadedKey)) {
                // @todo: Handle multi-value keys properly.
                // @todo Don't use concrete class
                return new Property(reset($loadedKey));
            }
            // @todo: Support creation of stubs when nothing found.
        }
        return null;
    }
}
