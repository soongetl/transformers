<?php
declare(strict_types=1);

namespace Soong\Transformer;

use Soong\Data\DataPropertyInterface;
use Soong\Data\Property;

/**
 * Transformer to lookup a value to be returned based on an input value.
 *
 * @todo: Handle failed lookups, something like:
 *   on_unmatched: # Provide exactly one of the following:
 *     default_value: blah
 *     use_original_value: true
 *     skip_transformation: true
 *     skip_record: true
 *
 * @package Soong\Transformer
 */
class ValueLookup implements TransformerInterface
{

    /**
     * {@inheritdoc}
     */
    public function transform(array $configuration, ?DataPropertyInterface $data) : ?DataPropertyInterface
    {
        return new Property($configuration['lookup_table'][$data->getValue()]);
    }
}
